import 'package:flutter/material.dart';
import 'package:trato_vip/widgets.dart';

class DrawerStyle extends StatefulWidget {
  @override
  _DrawerStyle createState() => _DrawerStyle();

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login',
      home: DrawerStyle(),
    );
  }
}

class _DrawerStyle extends State<DrawerStyle> {
  @override
  Widget build(BuildContext context) {
    AllWidgets w = AllWidgets();
    final edit = w.getDynamicLink(context, "editar", '/login');

    return Drawer(
        child: ListView(
      padding: EdgeInsets.all(0),
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Container(
                color: Color.fromRGBO(77, 135, 234, 1),
                child: Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 20, top: 50, bottom: 20),
                      height: 100,
                      width: 100,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.white),
                      child: Icon(
                        Icons.person,
                        size: 80,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Meu salão",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                          Container(
                            child: edit,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(
                        "Agendamentos",
                      ),
                      leading: Icon(
                        Icons.calendar_today,
                      ),
                      onTap: () {},
                    ),
                     ListTile(
                      title: Text(
                        "Relatório",
                      ),
                      leading: Icon(
                        Icons.assessment,
                      ),
                      onTap: () {},
                    ),
                     ListTile(
                      title: Text(
                        "Configurações",
                      ),
                      leading: Icon(
                        Icons.settings,
                      ),
                      onTap: () {},
                    ),
                     ListTile(
                      title: Text(
                        "Ajuda",
                      ),
                      leading: Icon(
                        Icons.help_outline,
                      ),
                      onTap: () {},
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    ));
  }
}
