import 'package:flutter/material.dart';
import 'package:trato_vip/drawer.dart';

class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login',
      home: Home(),
    );
  }
}

class _Home extends State<Home> {
  List horarios = [
    ["08h00", false],
    ["09h00", true],
    ["10h00", true],
    ["11h00", false],
    ["12h00", false],
    ["13h00", true],
    ["14h00", true]
  ];

  Widget getAppointmentDay(int day) {
    final actualDay = DateTime.now().weekday;

    bool today = false;

    if (actualDay == day) {
      today = true;
    }

    final daysOfWeek = ["DOM", "SEG", "TER", "QUA", "QUI", "SEX", "SAB"];

    return Container(
      margin: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          Text(
            daysOfWeek[day],
            style: TextStyle(color: Colors.white),
          ),
          Padding(
            padding: EdgeInsets.only(top: 5),
          ),
          Container(
            height: 50,
            width: 50,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: today ? Color(0xff027FC1) : Colors.white),
          ),
        ],
      ),
    );
  }

  Widget getAppointmentState(BuildContext context, int pos) {
    String time = horarios[pos][0];
    bool isActive = horarios[pos][1];

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: isActive ? Colors.red : Color.fromRGBO(68, 132, 222, 1),
        child: GestureDetector(
          onTap: () {
            setState(() {
              if (horarios[pos][1]) {
                horarios[pos][1] = false;
              } else {
                horarios[pos][1] = true;
              }
            });
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      color: Color.fromRGBO(77, 135, 234, 1),
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(right: 15),
                      height: 80,
                      width: 120,
                      child: Text(
                        time,
                        style: TextStyle(color: Colors.white, fontSize: 36),
                      ),
                    ),
                    Container(
                      width: isActive ? 2 : null,
                      height: 80,
                      color: Colors.white,
                      alignment: Alignment.topCenter,
                    )
                  ],
                ),
              ),
              Container(
                child: Text(
                  isActive ? "Horário indisponível" : "Agendar horário",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 20),
                child: Icon(
                  isActive ? Icons.delete : Icons.add,
                  color: Colors.white,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    var draw = DrawerStyle();

    List<Widget> horas = new List();

    for(var i = 0; i<horarios.length; i++ ){
      horas.add(getAppointmentState(context, i));
    }

    var homeBody = Container(
        child: ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "Agenda de hoje",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 24),
          ),
        ),
        Container(
          color: Colors.black12,
          width: MediaQuery.of(context).size.width,
          child: SizedBox(
            height: 0.5,
          ),
        ),
        Container(
          height: 100,
          margin: EdgeInsets.symmetric(horizontal: 20),
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              getAppointmentDay(0),
              getAppointmentDay(1),
              getAppointmentDay(2),
              getAppointmentDay(3),
              getAppointmentDay(4),
              getAppointmentDay(5),
              getAppointmentDay(6),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 20),
          child: Column(
            children: horas
          ),
        ),
      ],
    ));

    return Scaffold(
      backgroundColor: Color.fromRGBO(77, 135, 234, 1),
      appBar: AppBar(
          centerTitle: true,
          title: Text(
            "trato.vip",
            style: TextStyle(
                color: Colors.white, fontSize: 48, fontWeight: FontWeight.w100),
          ),
          elevation: 0,
          backgroundColor: Color.fromRGBO(77, 135, 234, 1),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Icon(
                Icons.settings,
              ),
            )
          ]),
      drawer: draw,
      body: homeBody,
    );
  }
}
