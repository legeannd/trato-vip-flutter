import 'package:flutter/material.dart';

class HomeWidgets extends StatefulWidget {
  HomeWidgets({Key key}) : super(key: key);

  Widget getAppointmentDay(int day) {
    final actualDay = DateTime.now().weekday;

    bool today = false;

    if (actualDay == day) {
      today = true;
    }

    final daysOfWeek = ["DOM", "SEG", "TER", "QUA", "QUI", "SEX", "SAB"];

    return Container(
      margin: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          Text(
            daysOfWeek[day],
            style: TextStyle(color: Colors.white),
          ),
          Padding(
            padding: EdgeInsets.only(top: 5),
          ),
          Container(
            height: 50,
            width: 50,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: today ? Color(0xff027FC1) : Colors.white),
          ),
        ],
      ),
    );
  }

  Widget getAppointmentState(BuildContext context, bool isActive, String time) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: Container(
          width: MediaQuery.of(context).size.width,
          color: isActive? Colors.red : Color.fromRGBO(68, 132, 222, 1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      color: Color.fromRGBO(77, 135, 234, 1),
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(right: 15),
                      height: 80,
                      width: 120,
                      child: Text(
                        time,
                        style: TextStyle(color: Colors.white, fontSize: 36),
                      ),
                    ),
                    Container(
                      width: isActive? 2 : null,
                      height: 80,
                      color: Colors.white,
                      alignment: Alignment.topCenter,
                    )
                  ],
                ),
              ),
              Container(
                child: Text(
                  isActive? "Horário indisponível" : "Agendar horário",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 20),
                child: GestureDetector(
                onTap: () {
                  isActive = !isActive;
                  print("foi apertado no horário "+time);
                },
                child: Icon(
                  isActive? Icons.delete : Icons.add,
                  color: Colors.white,
                ),
              )
              )
            ],
          ),
        ),
      );
    
  }

  _HomeWidgetsState createState() => _HomeWidgetsState();
}

class _HomeWidgetsState extends State<HomeWidgets> {

  Widget build(BuildContext context) {
    return Container( );
  }
}
