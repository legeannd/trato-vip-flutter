import 'package:flutter/material.dart';
import 'package:trato_vip/widgets.dart';

class TelaLogin extends StatefulWidget {
  TelaLogin({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _TelaLogin createState() => _TelaLogin();

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login',
      home: TelaLogin(),
    );
  }
}

class _TelaLogin extends State<TelaLogin> {
  @override
  Widget build(BuildContext context) {
    AllWidgets w = AllWidgets();

    final loginsAvatar = w.getLoginsAvatar(150.0);

    final socialLogin = w.getSocialLogin();

    final emailField = w.getDynamicField("Usuário ou e-mail", false);

    final passwordField = w.getDynamicField("Senha", true);

    final loginButton = w.getDynamicButton(context, "Entrar", "/home");

    final forgotPass = w.getDynamicLink(context, "Esqueceu a senha?", "/recovery");

    final signIn = w.getDynamicLink(context, "Não tem uma conta? Cadastre-se", "/sign_in");

    return Scaffold(
      backgroundColor: Color(0xff508aed),
      body: Center(
          child: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              loginsAvatar,
              SizedBox(
                height: 10,
              ),
              socialLogin,
              SizedBox(
                height: 25,
              ),
              Container(
                margin: EdgeInsets.only(left: 50, right: 50),
                child: emailField,
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 50, right: 50),
                child: passwordField,
              ),
              SizedBox(
                height: 25,
              ),
              loginButton,
              SizedBox(
                height: 20,
              ),
              forgotPass,
              SizedBox(
                height: 15,
              ),
              signIn
            ],
          ),
        ),
      )),
    );
  }
}
