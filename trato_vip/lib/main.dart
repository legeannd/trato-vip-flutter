import 'package:flutter/material.dart';
import 'package:trato_vip/signIn.dart';
import 'package:trato_vip/login.dart';
import 'package:trato_vip/recoveryPass.dart';
import 'package:trato_vip/home.dart';

void main() => runApp(TratoVip());

class TratoVip extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Trato.vip',
      debugShowCheckedModeBanner: false,
      home: TelaLogin(),
      routes: <String, WidgetBuilder>{
        '/sign_in': (BuildContext context) => SignIn(),
        '/login': (BuildContext context) => TelaLogin(),
        '/recovery': (BuildContext context) => RecoveryPassword(),
        '/home': (BuildContext context) => Home(),
      },
    );
  }
}
