import 'package:flutter/material.dart';
import 'package:trato_vip/widgets.dart';
import 'dart:async';

class RecoveryPassword extends StatefulWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Recovery Password",
      home: RecoveryPassword(),
    );
  }

  @override
  _RecoveryPassword createState() => _RecoveryPassword();
}

class _RecoveryPassword extends State<RecoveryPassword> {
  DateTime selectedDate = DateTime.now();
  bool flag = false;
  TextEditingController dateController = TextEditingController();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        initialDatePickerMode: DatePickerMode.year,
        firstDate: DateTime(1920),
        lastDate: DateTime(2030));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked.toLocal();
        flag = true;
      });
  }

  @override
  Widget build(BuildContext context) {
    AllWidgets w = AllWidgets();

    final loginsAvatar = w.getLoginsAvatar(150.0);

    final socialLogin = w.getSocialLogin();

    final emailField = w.getDynamicField("Usuário ou e-mail", false);

    final recoveryButton = w.getDynamicButton(context, "Recuperar", "/home");

    final login =
        w.getDynamicLink(context, "Tem uma conta? Faça login", "/login");

    final signIn = w.getDynamicLink(
        context, "Não possui uma conta? Cadastre-se", "/sign_in");

    final birthDate = GestureDetector(
      onTap: () => _selectDate(context),
      child: AbsorbPointer(
          child: TextFormField(
        style: TextStyle(color: Colors.white),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: flag 
              ? "${selectedDate.day}/${selectedDate.month}/${selectedDate.year}"
              : "Data de nascimento",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          fillColor: Color(0xff93b1e3),
          filled: true,
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
        ),
      )),
    );

    return Scaffold(
      backgroundColor: Color(0xff508aed),
      body: Center(
          child: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              loginsAvatar,
              SizedBox(
                height: 10,
              ),
              socialLogin,
              SizedBox(
                height: 25,
              ),
              Container(
                margin: EdgeInsets.only(left: 50, right: 50),
                child: emailField,
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 50, right: 50),
                child: birthDate,
              ),
              SizedBox(
                height: 25,
              ),
              recoveryButton,
              SizedBox(
                height: 20,
              ),
              login,
              SizedBox(
                height: 15,
              ),
              signIn
            ],
          ),
        ),
      )),
    );
  }
}
