import 'package:flutter/material.dart';
import 'package:trato_vip/widgets.dart';

class SignIn extends StatefulWidget {
  @override
  SignIn({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SignIn createState() => _SignIn();

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sign In',
      home: SignIn(),
    );
  }
}

class _SignIn extends State<SignIn> {
  @override
  Widget build(BuildContext context) {
    AllWidgets w = AllWidgets();

    final loginsAvatar = w.getLoginsAvatar(150.0);

    final socialLogin = w.getSocialLogin();

    final emailField = w.getDynamicField("Usuário ou e-mail", false);

    final passwordField = w.getDynamicField("Senha", true);

    final confirmPass = w.getDynamicField("Confirme a senha", true);

    final signInButton = w.getDynamicButton(context, "Cadastrar", "/home");

    final login = w.getDynamicLink(context, "Já tem uma conta? Faça login", "/login");

    return Scaffold(
      backgroundColor: Color(0xff508aed),
      body: Center(
          child: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              loginsAvatar,
              SizedBox(
                height: 10,
              ),
              socialLogin,
              SizedBox(
                height: 25,
              ),
              Container(
                margin: EdgeInsets.only(left: 50, right: 50),
                child: emailField,
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 50, right: 50),
                child: passwordField,
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.only(left: 50, right: 50),
                child: confirmPass,
              ),
              SizedBox(
                height: 25,
              ),
              signInButton,
              SizedBox(
                height: 20,
              ),
              login,
            ],
          ),
        ),
      )),
    );
  }
}
