import 'package:flutter/material.dart';

class AllWidgets {
  Widget getLoginsAvatar(height) {
    return SizedBox(
      height: height,
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
            image: DecorationImage(
              image: AssetImage('assets/razor.png'),
              fit: BoxFit.contain,
            )),
      ),
    );
  }

  Widget getSocialLogin() {
    return Container(
      child: Column(
        children: <Widget>[
          Text(
            "Fazer login com:",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                child: SizedBox(
                  height: 32,
                  width: 32,
                  child: Image.asset(
                    'assets/google-plus.png',
                    fit: BoxFit.contain,
                  ),
                ),
                onTap: () {},
              ),
              SizedBox(
                width: 10,
              ),
              GestureDetector(
                child: SizedBox(
                  height: 32,
                  width: 32,
                  child: Image.asset(
                    'assets/facebook.png',
                    fit: BoxFit.contain,
                  ),
                ),
                onTap: () {},
              )
            ],
          )
        ],
      ),
    );
  }

  Widget getDynamicField(hint, isObscure) {
    return TextFormField(
      obscureText: isObscure,
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: hint,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        fillColor: Color(0xff93b1e3),
        filled: true,
        hintStyle: TextStyle(color: Colors.white),
        enabledBorder:
            OutlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
      ),
    );
  }

  Widget getDynamicButton(context, label, route) {
    return Material(
      color: Color(0xff4a86e8),
      borderRadius: BorderRadius.circular(5),
      child: OutlineButton(
        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        onPressed: () {
          Navigator.pushNamed(context, route);
        },
        child: Text(
          label,
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        borderSide: BorderSide(color: Colors.white),
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
      ),
    );
  }

  Widget getDynamicLink(context, label, route) {
    return GestureDetector(
      child: Text(
        label,
        style: TextStyle(
            color: Colors.white, decoration: TextDecoration.underline),
      ),
      onTap: () {
        Navigator.pushNamed(context, route);
      },
    );
  }
}
